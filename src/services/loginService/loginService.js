/**
 * Created by Tamir on 29/06/14.
 */
angular.module('App.services')
    .service("loginService", ['loginServiceMock', 'Restangular', '$rootScope', '$ionicLoading', function(loginServiceMock, Restangular, $rootScope, $ionicLoading) {
        var self = this;
        var SO_TOKEN_KEY = "solocalstorgeobject";
        self.Api = {};

        self._parseData = function(rawCards) {
            var result = rawCards;
            return result;
        };

        self.getUser = function(callback){
            var savedUserStr = window.localStorage.getItem(SO_TOKEN_KEY);
            if(savedUserStr){
                var savedUser = JSON.parse(savedUserStr);
                callback(savedUser);
            }
            else{
                if($rootScope.isMocked) {
                    callback(loginServiceMock.login());
                }
                else {
                    console.log("loginService: get user failed");
                    callback(null);
                }

            }
        };

        self.login = function(userDetails, callback) {
            self.Api = Restangular.all('login');
            $ionicLoading.show({
                template: $rootScope.loadingTemplate
            });
            self.Api.post(userDetails).then(function(res){
                window.localStorage.setItem(SO_TOKEN_KEY, JSON.stringify(res));
                $rootScope.user = res;
                Restangular.setDefaultHeaders({
                    Authorization: "Token " + res.token
                });
                $rootScope.$broadcast("cacheRefresh");
                $ionicLoading.hide();
                callback();
            }, function(err){
                $ionicLoading.hide();
                callback(err);
            });
        };

        self.switchAccount = function(currentAccount, callback) {
            self.Api = Restangular.one('me');
            self.Api.customPOST(
                {
                    "current_account": {
                        "type": currentAccount.type,
                        "id": currentAccount.id
                    }
                })
                .then(function(res){
                    res.token = $rootScope.user.token;
                    window.localStorage.setItem(SO_TOKEN_KEY, JSON.stringify(res));
                    $rootScope.$broadcast("cacheRefresh");
                    $rootScope.user = res;
                    callback(res);
                }, function(err){
                    if($rootScope.isMocked) {
                        callback(loginServiceMock.login());
                    }
                    else {
                        console.log("loginService: switch account failed");
                        callback(null);
                    }
                    //alert("loginService failed in login: " + err.status.toString());


                });
        };

        self.signOut = function() {
            window.localStorage.removeItem(SO_TOKEN_KEY);
            $rootScope.$broadcast("cacheRefresh");
            $rootScope.user = null;
        };
    }]);