/**
 * Created by Tamir on 09/07/14.
 */
angular.module('App.common', [])
    .directive('changeAmount', function() {
        return {
            restrict: "E",
            templateUrl: "changeAmount/changeAmount.tpl.html",
            scope: {
                product: "=",
                countingItem: "@",
                countingLabel: "=",
                modified: "&",
                modified2: "&ngClick"
            },
            link: function(scope, element, attr) {

                function updateProduct(value) {
                    scope.product = parseFloat(scope.product) + parseFloat(value);
                    scope.product = +scope.product.toFixed(2);
                    if (scope.modified) {
                        scope.modified({value: scope.product});
                    }
                }

                scope.increaseQuantity = function() {
                    updateProduct(1);
                };
                scope.decreaseQuantity = function() {
                    if(scope.product >= 1) {
                        updateProduct(-1);
                    }
                };
                if(scope.countingLabel === undefined) {
                    element.addClass('centerInput');
                }

            }
        };
    });
