/**
 * Created by Tamir on 26/06/14.
 */
angular.module('App.common')
    .controller("SideMenuController", ['$scope', '$rootScope', '$state', 'authorizationService', 'loginService', '$ionicSideMenuDelegate', '$stateParams',
        function ($scope, $rootScope, $state, authorizationService, loginService, $ionicSideMenuDelegate, $stateParams) {
            $scope.selectedAccount = {};
            $scope.multipleAccounts = false;

            /*if multipleAccounts = flase, than isCollapsed should be set to false as well (in order to show the list)*/
            $scope.isCollapsed = false;
            $scope.accounts = [];
            $rootScope.$watch('user', $scope.refreshState);

            $scope.refreshState = function() {
                var profile = $rootScope.user;
                if(profile){
                    $scope.accounts = [];
                    for(var i = 0; i < profile.profile.accounts.length; i++) {
                        if(profile.profile.accounts[i].type == "customer") {
                            $scope.accounts.push(profile.profile.accounts[i]);
                        }
                    }
                    if($scope.accounts.length > 1) {
                        $scope.multipleAccounts = true;
                        $scope.isCollapsed = true;
                    }
                    $scope.selectedAccount = _.findWhere($scope.accounts, {id: profile.profile.current_account.id});
                }
            };

            /*update server with chosen account*/
            $scope.updateSelectedAccount = function(account) {
                loginService.switchAccount(account, function(profile) {
	                $state.go($state.current, $stateParams, {
		                reload: true
	                });
                });
            };

            $scope.isStateLinkAvailable = function(stateName) {
                return authorizationService.isStateInUserAcl($scope.user,stateName);
            };

            $scope.closeMenu = function(state){
                $ionicSideMenuDelegate.toggleLeft(false);
                $state.go(state, {}, {
                    reload: true
                });
            };

            // langunge section
            $scope.changeLanguage = function(langunge) {
                if(langunge == "he"){
                    $rootScope.isHebrew = true;
                }
                else{
                    $rootScope.isHebrew = false;
                }
                $translate.use(langunge);
            };


            $scope.signOut = function(){
                loginService.signOut();
                $state.go("login", {}, {
                    reload: true
                });
            };

            $scope.refreshState();
    }]);
