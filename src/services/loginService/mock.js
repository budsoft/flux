angular.module('App.mocks', [])
    .factory("loginServiceMock", function () {
        this.login = function() {
            var userDeatils = {
                "status": "ok",
                "user_id": "1",
                "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoiMTM3NTQiLCJpYXQiOjE0MDUyNTM5MDAsIm5iZiI6MTQwNTI1NzUwMCwic2Vzc2lvbl9pZCI6Im9lbDc0YnZxbmUxbnBuMDhqZ25rMTY2NGQwIn0.tVNhIapAXFumXpU6DUZJXswpCXLox0U_y_4dSYFUcuU",
                "lang": {
                    "code": "en",
                    "is_rtl": false
                },
                "profile": {
                    "user": {
                        "id": "1",
                        "name": "SimpleOrder Team",
                        "first_name": "SimpleOrder",
                        "is_admin": true
                    },
                    "current_account": {
                        "type": "customer",
                        "id": 1369,
                        "name": "Israeli",
                        "logo": "images\/logo_supplier.ltr.png",
                        "acl": {
                            "view_prices": true,
                            "order": true,
                            "sent_orders": true,
                            "templates": true,
                            "drafts": true,
                            "inventory": true,
                            "suppliers": true,

                            "branches": true,
                            "bom": true,
                            "reports": {
                                "dashboard": true,
                                "products": true,
                                "tags": true,
                                "payments": true,
                                "foodcost": true,
                                "today": true
                            }
                        }
                    },
                    "accounts": [
                        {
                            "type": "customer",
                            "id": 1,
                            "name": "Einstein Cafe",
                            "logo": "images\/customers\/logo\/1.1402382389.png"
                        },
                        /*{
                         "type": "customer",
                         "id": 1,
                         "name": "Einstein Cafe",
                         "logo": "images\/customers\/logo\/1.1402382389.png"
                         },
                         {
                         "type": "customer",
                         "id": 1,
                         "name": "Einstein Cafe",
                         "logo": "images\/customers\/logo\/1.1402382389.png"
                         },*/
                        {
                            "type": "supplier",
                            "id": 13697,
                            "name": "Coca-Cola",
                            "logo": "images\/logo_supplier.ltr.png"
                        }
                    ],
                    "request_token": "d30da6e4ce0ae931a85d64b72b0e453a"
                }
            };

            return userDeatils;
        };
        return this;
    });