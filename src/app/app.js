/**
 * Created by Tamir on 18/02/14.
 */


var App = angular.module("App", [
    'templates-app',
    'templates-common',
    'ui.router',
    'ionic',
    'ngAnimate',
    'restangular',
    'truncate',
    'App.routes',
    'App.services',
    'App.common',
    'ui.bootstrap'
])
    .config(function (RestangularProvider) {
        RestangularProvider.setBaseUrl('https://m2.simpleorder.co/rest');
    })
    .controller("AppCtrl", ["$scope", "$rootScope", "loginService", "$ionicSideMenuDelegate", "Restangular", '$state', 'authorizationService',
        function ($scope, $rootScope, loginService, $ionicSideMenuDelegate, Restangular, $state, authorizationService) {
            /*$rootScope.isMocked = true;*/
            //$rootScope.loadingTemplate = $translate.instant('loadingText');
            $rootScope.loadingTemplate = "Loading...";
            $rootScope.isHebrew = false;
            $rootScope.delay = 200;

            $scope.isDetailsShown = false;

            $scope.showDetails = function(value,e){
                $('.panel-body.selected').removeClass('selected');
                $(e.target).parents('.panel-body').addClass('selected');

                $scope.generateDetails(value);

                $scope.isDetailsShown = true;
            };

            $scope.generateDetails= function(type){
                $scope.detailsContainer = {
                    label: "test",
                    value: "1234"
                };
            };

            loginService.getUser(function (user) {
                if(user){
                    $scope.profile = user.profile;
                    $rootScope.user = user;
                }
            });

            $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {
                //we climb up the states until we find authentication or we found none.
                authorizationService.isUserAuthorizedToState(toState.name,function (isAuth) {
                    if (isAuth === false) {
                        console.log("preventing user from switching to state due to authorization");
                        event.preventDefault();
                    }
                });
                /*close side menu when state is changes*/
                $ionicSideMenuDelegate.toggleLeft(false);
            });

            document.addEventListener("deviceready", onDeviceReady, false);
            function onDeviceReady() {
                navigator.splashscreen.hide();
            }
        }]);
