/**
 * This file/module contains all configuration for the build process.
 */
module.exports = {
  /**
   * The `build_dir` folder is where our projects are compiled during
   * development and the `compile_dir` folder is where our app resides once it's
   * completely built.
   */
  build_dir: 'www',
  res_dir: 'src/res',
  compile_dir: 'bin',

  /**
   * This is a collection of file patterns that refer to our app code (the
   * stuff in `src/`). These file paths are used in the configuration of
   * build tasks. `js` is all project javascript, less tests. `ctpl` contains
   * our reusable components' (`src/common`) template HTML files, while
   * `atpl` contains the same, but for our app's code. `html` is just our
   * main HTML file, `sass` is our main stylesheet, and `unit` contains our
   * app's unit tests.
   */
  app_files: {
    json: ['src/**/*.json'],
    js: ['src/**/*.js', '!src/**/*.spec.js', '!src/assets/**/*.js'],
    jsunit: ['src/**/*.spec.js'],

    coffee: ['src/**/*.coffee', '!src/**/*.spec.coffee'],
    coffeeunit: ['src/**/*.spec.coffee'],

    atpl: ['src/app/**/*.tpl.html'],
    ctpl: ['src/common/**/*.tpl.html'],

    html: ['src/index.html'],
    sass: 'src/sass/main.scss',

    ios_assets: [
        {src: ['www/res/icon/ios/icon-57.png'], dest: 'platforms/ios/<%= app.name %>/Resources/icons/icon.png'},
        {src: ['www/res/icon/ios/icon-57-2x.png'], dest: 'platforms/ios/<%= app.name %>/Resources/icons/icon@2x.png'},
        {src: ['www/res/icon/ios/icon-72.png'], dest: 'platforms/ios/<%= app.name %>/Resources/icons/icon-72.png'},
        {src: ['www/res/icon/ios/icon-72-2x.png'], dest: 'platforms/ios/<%= app.name %>/Resources/icons/icon-72@2x.png'},
        {src: ['www/res/screen/ios/screen-iphone-portrait.png'], dest: 'platforms/ios/<%= app.name %>/Resources/splash/Default~iphone.png'},
        {src: ['www/res/screen/ios/screen-iphone-portrait-2x.png'], dest: 'platforms/ios/<%= app.name %>/Resources/splash/Default@2x~iphone.png'},
        {src: ['www/res/screen/ios/screen-iphone-portrait-568h-2x.png'], dest: 'platforms/ios/<%= app.name %>/Resources/splash/Default-568h@2x~iphone.png'},
        {src: ['www/res/screen/ios/screen-ipad-portrait.png'], dest: 'platforms/ios/<%= app.name %>/Resources/splash/Default-Portrait~ipad.png'},
        {src: ['www/res/screen/ios/screen-ipad-portrait-2x.png'], dest: 'platforms/ios/<%= app.name %>/Resources/splash/Default-Portrait@2x~ipad.png'},
        {src: ['www/res/screen/ios/screen-ipad-landscape.png'], dest: 'platforms/ios/<%= app.name %>/Resources/splash/Default-Landscape~ipad.png'},
        {src: ['www/res/screen/ios/screen-ipad-landscape-2x.png'], dest: 'platforms/ios/<%= app.name %>/Resources/splash/Default-Landscape@2x~ipad.png'}
    ],
    android_assets: [
        {src: ['www/res/icon/android/icon-36-ldpi.png'], dest: 'platforms/android/res/drawable-ldpi/icon.png'},
        {src: ['www/res/icon/android/icon-48-mdpi.png'], dest: 'platforms/android/res/drawable-mdpi/icon.png'},
        {src: ['www/res/icon/android/icon-72-hdpi.png'], dest: 'platforms/android/res/drawable-hdpi/icon.png'},
        {src: ['www/res/icon/android/icon-96-xhdpi.png'], dest: 'platforms/android/res/drawable-xhdpi/icon.png'},
        {src: ['www/res/icon/android/icon-96-xhdpi.png'], dest: 'platforms/android/res/drawable/icon.png'},
        {src: ['www/res/screen/android/screen-ldpi-portrait.png'], dest: 'platforms/android/res/drawable-ldpi/screen.png'},
        {src: ['www/res/screen/android/screen-mdpi-portrait.png'], dest: 'platforms/android/res/drawable-mdpi/screen.png'},
        {src: ['www/res/screen/android/screen-hdpi-portrait.png'], dest: 'platforms/android/res/drawable-hdpi/screen.png'},
        {src: ['www/res/screen/android/screen-xhdpi-portrait.png'], dest: 'platforms/android/res/drawable-xhdpi/screen.png'},
        {src: ['www/res/screen/android/screen-xhdpi-portrait.png'], dest: 'platforms/android/res/drawable/screen.png'}
    ],

    cordova_plugins: [
        'org.apache.cordova.console',
        'org.apache.cordova.device',
        'org.apache.cordova.network-information',
        'org.apache.cordova.splashscreen'
    ],

    cordova_icon: 'res/icon.png',
    cordova_screen: 'res/screen.png'
  },

  /**
   * This is the same as `app_files`, except it contains patterns that
   * reference vendor code (`vendor/`) that we need to place into the build
   * process somewhere. While the `app_files` property ensures all
   * standardized files are collected for compilation, it is the user's job
   * to ensure non-standardized (i.e. vendor-related) files are handled
   * appropriately in `vendor_files.js`.
   *
   * The `vendor_files.js` property holds files to be automatically
   * concatenated and minified with our project source files.
   *
   * The `vendor_files.css` property holds any CSS files to be automatically
   * included in our app.
   *
   * The `vendor_files.assets` property holds any assets to be copied along
   * with our app's assets. This structure is flattened, so it is not
   * recommended that you use wildcards.
   */
  vendor_files: {
    js: [
        'vendor/ionic/release/js/ionic.bundle.js',
        'vendor/angular-animate/angular-animate.js',
        'vendor/angular-ui-router/angular-ui-router.js',
        'vendor/underscore/underscore.js',
        'vendor/slip/slip.js',
        'vendor/restangular/dist/restangular.js',
        'vendor/angular-truncate/src/truncate.js',
        'vendor/angular-bootstrap/ui-bootstrap.js',
        'vendor/angular-bootstrap/ui-bootstrap-tpls.js',
        'vendor/angular-elastic/elastic.js',
        'vendor/async/lib/async.js',
        'vendor/moment/min/moment-with-langs.min.js',
        'vendor/angular-translate/angular-translate.min.js',
        'vendor/angular-translate-loader-static-files/angular-translate-loader-static-files.min.js',
        'vendor/jquery/dist/jquery.js',
            'vendor/jquery-browser-mobile/jquery.browser.mobile.js',
            'vendor/jquery-cookie/jquery.cookie.js',
            'vendor/style-switcher/style.switcher.js',
            'vendor/bootstrap/js/bootstrap.js',
            'vendor/nanoscroller/nanoscroller.js',
            'vendor/bootstrap-datepicker/js/bootstrap-datepicker.js',
            'vendor/magnific-popup/magnific-popup.js',
            'vendor/jquery-placeholder/jquery.placeholder.js',
            'vendor/jquery-ui/js/jquery-ui-1.10.4.custom.js',
            'vendor/jquery-ui-touch-punch/jquery.ui.touch-punch.js',
            'vendor/jquery-appear/jquery.appear.js',
            'vendor/bootstrap-multiselect/bootstrap-multiselect.js',
            'vendor/jquery-easypiechart/jquery.easypiechart.js',
            'vendor/flot/jquery.flot.js',
            'vendor/flot-tooltip/jquery.flot.tooltip.js',
            'vendor/flot/jquery.flot.pie.js',
            'vendor/flot/jquery.flot.categories.js',
            'vendor/flot/jquery.flot.resize.js',
            'vendor/jquery-sparkline/jquery.sparkline.js',
            'vendor/raphael/raphael.js',
            'vendor/morris/morris.js',
            'vendor/modernizr/modernizr.js',
            'vendor/gauge/gauge.js',
            'vendor/snap-svg/snap.svg.js',
            'vendor/liquid-meter/liquid.meter.js',
            'vendor/jqvmap/jquery.vmap.js',
            'vendor/jqvmap/data/jquery.vmap.sampledata.js',
            'vendor/jqvmap/maps/jquery.vmap.world.js',
            'vendor/jqvmap/maps/continents/jquery.vmap.africa.js',
            'vendor/jqvmap/maps/continents/jquery.vmap.asia.js',
            'vendor/jqvmap/maps/continents/jquery.vmap.australia.js',
            'vendor/jqvmap/maps/continents/jquery.vmap.europe.js',
            'vendor/jqvmap/maps/continents/jquery.vmap.north-america.js',
            'vendor/jqvmap/maps/continents/jquery.vmap.south-america.js',
            'src/assets/javascripts/theme.js',
            'src/assets/javascripts/theme.custom.js',
            'src/assets/javascripts/theme.init.js',
            'src/assets/javascripts/dashboard/examples.dashboard.js'
    ],
    css: [],
    assets: []
  }
};
