
angular.module('App.services')
    .service("httpWrapperService", ['$http', '$rootScope', '$q', '$ionicLoading', function($http, $rootScope, $q, $ionicLoading){
        var self = this;
        var baseUrl = "https://m2.simpleorder.co/ajax/";

        var httpCallbackToPromise = function(methodName, url, data, showLoader){
            var deferred = $q.defer();
            var promise = deferred.promise;

            //default value of data {}, in case no data.
            data = data !== undefined ? data : {};
	        if($rootScope.user){
		        data.request_token = $rootScope.user.profile.request_token;
		        $http.defaults.headers.common.Authorization = 'Token ' + $rootScope.user.token;
	        }
            if(showLoader){
                $ionicLoading.show({
                    template: $rootScope.loadingTemplate
                });
            }
            if(methodName === "get"){
                data = {params: data};
            }
            $http[methodName](url, data)
                .success(function(responseData) {
                    deferred.resolve(responseData);
                    if(showLoader){
                        $ionicLoading.hide();
                    }
                })
                .error(function(responseData) {
                    deferred.reject(responseData);
                    if(showLoader){
                        $ionicLoading.hide();
                    }
                });
            return promise;
        };

        self.getAjaxAPI = function(url, showLoader) {
            var newUrl = baseUrl + url;
            return {
                post: function(data){
                    return httpCallbackToPromise('post', newUrl, data, showLoader);
                },
                get: function(data){
                    return httpCallbackToPromise('get', newUrl, data, showLoader);
                },
                remove: function(data){
                    return httpCallbackToPromise('delete', newUrl, data, showLoader);
                },
                put: function(data){
                    return httpCallbackToPromise('put', newUrl, data, showLoader);
                },
                delete: function(data){
                    return httpCallbackToPromise('delete', newUrl, data, showLoader);
                }
            };
        };
    }]);