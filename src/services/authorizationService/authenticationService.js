/**
 * Created by Eliran on 06/07/14.
 */
angular.module('App.services', ['App.mocks', 'restangular'])
    .service("authorizationService", ['$state','loginService', function ($state,loginService) {
        var self = this;

        self.isStateInUserAcl = function (user,stateName) {
            if(user){
                var userAccount = user.profile.current_account;
                var currentState = $state.get(stateName);

                while (currentState) {
                    if (currentState.authentication) {
                        if (userAccount.acl[currentState.authentication] !== true) {
                            return false;
                        }
                        break;
                    }
                    var parentStateName = _.initial(currentState.name.split("."), 1).join(".");
                    if (parentStateName === "") {
                        break;
                    }
                    currentState = $state.get(parentStateName);
                }
            }

            return true;
        };

        self.isUserAuthorizedToState = function (stateName,callback) {
            loginService.getUser(function (user) {
                var retValue = self.isStateInUserAcl(user,stateName);
                callback(retValue);
            });
        };

    }]
);