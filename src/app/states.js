/**
 * Created by Tamir on 09/03/14.
 */
angular.module('App.routes', [
    'ui.router',
    'App.login'
])

    .config(function ($stateProvider, $urlRouterProvider) {
        $stateProvider.state("root", {
            abstract: true,
            templateUrl: "main.tpl.html"
        })
        /* login stuff */
        .state("base", {
            parent: "root",
            url: "/",
            views: {
                "main": {
                    templateUrl: "main.tpl.html"
                }
            }
        });
        $urlRouterProvider.otherwise("/");
    });